mongoscript="[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf
Restart=always
RestartSec=10


[Install]
WantedBy=multi-user.target";

sortConfig="setParameter:
  internalQueryExecMaxBlockingSortBytes: 268435456";

sudo apt-get install curl

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org

sudo echo "$sortConfig" | sudo tee /etc/mongod.conf -a

sudo echo "$mongoscript" > /etc/systemd/system/mongodb.service

sudo systemctl start mongodb

sudo systemctl enable mongodb